import ai.*;
import battleship.GameMain;
import battleship.Renderer;

import java.util.Random;

/**
 * Created by Nathaniel on 12/11/2014
 * for Battleship
 */
public class Main {
	public static void main(String[] args) throws InterruptedException {
		Renderer r = new Renderer();
		GameMain g;
		if(args.length == 0) {
			g = new GameMain();
			r.setGame(g);
			playGame(250, g);
			System.out.println("Game won in " + g.getShootCount());
		} else if(args.length == 1) {
			int high = Integer.MIN_VALUE;
			int low = Integer.MAX_VALUE;
			int passCount = 0;
			int average = 0;
			int gamesToPlay = Integer.parseInt(args[0]);
			for(int x = 0; x < gamesToPlay; x++) {
				g = new GameMain();
				r.setGame(g);
				playGame(0, g);
				Thread.sleep(5);
				System.out.println(String.format("%d of %d", x, gamesToPlay));
				if(g.getShootCount() > high) {
					high = g.getShootCount();
				} else if(g.getShootCount() < low) {
					low = g.getShootCount();
				}
				if(g.getShootCount() <= 65) {
					passCount ++;
				}
				average += g.getShootCount();
			}
			average /= gamesToPlay;
			System.out.print(
					String.format(
							"Results from: %d\nHigh: %d\nLow: %d\nAverage: %d\nWon: %d of %d\n"
							,gamesToPlay, high, low, average, passCount, gamesToPlay
					)
			);
		}
	}

	private static AiInterface getAI() {
		return new BattleAI();
	}

	public static int playGame(int turnDelay, GameMain g) {
		AiInterface ai = getAI();
		try {
			while(!g.isOver()) {
				Thread.sleep(turnDelay);
				Shot shot = ai.getShot();
				GameMain.ShootResult result = g.shoot(shot.getX(), shot.getY());
				ai.handleResult(convertResult(result));
				if(result != null) {
				}
			}
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		return g.getShootCount();
	}

	public static ShotResult convertResult(GameMain.ShootResult result) {
		return new ShotResult(convertShot(result.getCoords()), result.getTile() == GameMain.MissileTile.HIT, convertShipType(result.getShip()));
	}

	public static Shot convertShot(GameMain.SimpleVector coords) {
		return new Shot(coords.getX(), coords.getY());
	}

	public static ShipType convertShipType(GameMain.Ship ship) {
		if(ship == GameMain.Ship.AIRCRAFT_CARRIER) {
			return ShipType.AIRCRAFT_CARRIER;
		} else if(ship == GameMain.Ship.BATTLESHIP) {
			return ShipType.BATTLESHIP;
		} else if(ship == GameMain.Ship.DESTROYER) {
			return ShipType.DESTROYER;
		} else if(ship == GameMain.Ship.SUBMARINE) {
			return ShipType.SUBMARINE;
		} else if(ship == GameMain.Ship.PATROL_BOAT) {
			return ShipType.PATROL_BOAT;
		} else {
			return null;
		}
	}
}