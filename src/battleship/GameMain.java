package battleship;

import processing.core.PApplet;
import processing.core.PConstants;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Vector;

/**
 * Created by Nathaniel on 12/11/2014
 * for Battleship
 */
public class GameMain {
	private MissileTile[][] missileTiles = new MissileTile[10][10];
	private Map<Ship, ShipPos> shipLocations = new HashMap<Ship, ShipPos>();
	private int shootCount = 0;

	public GameMain() {
		for(int i = 0; i < missileTiles.length; i++) {
			for(int j = 0; j < missileTiles[i].length; j++) {
				missileTiles[i][j] = MissileTile.EMPTY;
			}
		}
		placeShips();
	}

	public int getShootCount() {
		return shootCount;
	}

	private void placeShips() {
		Random r = new Random();
		SimpleVector testSpot;
		SimpleVector testSpot2;
		ShipPos testShipPos;

		for(Ship s: Ship.values()) {
			while(true) {
				testSpot = new SimpleVector(r.nextInt(10), r.nextInt(10));
				testSpot2 = r.nextBoolean() ?
						new SimpleVector(testSpot.x, r.nextBoolean() ? testSpot.y - s.size + 1 : testSpot.y + s.size - 1) :
						new SimpleVector(r.nextBoolean() ? testSpot.x - s.size + 1 : testSpot.x + s.size - 1, testSpot.y);
				testShipPos = new ShipPos(testSpot.x, testSpot.y, testSpot2.x, testSpot2.y);
				if(!doesHitsShip(testShipPos) && isOnScreen(testShipPos)) {
					shipLocations.put(s, testShipPos);
					break;
				}
			}
		}
	}

	public MissileTile[][] getMissileTiles() {
		return missileTiles;
	}

	public Map<Ship, ShipPos> getShipLocations() {
		return shipLocations;
	}

	private boolean isOnScreen(ShipPos testSpot) {
		if(testSpot.pos1.x < 0 || testSpot.pos1.x > 9) {
			return false;
		}
		if(testSpot.pos1.y < 0 || testSpot.pos1.y > 9) {
			return false;
		}
		if(testSpot.pos2.x < 0 || testSpot.pos2.x > 9) {
			return false;
		}
		if(testSpot.pos2.y < 0 || testSpot.pos2.y > 9) {
			return false;
		}
		return true;
	}

	private boolean doesHitsShip(ShipPos testSpot) {
		SimpleVector[] testSpotTiles = getShipsTiles(testSpot);
		for(ShipPos sp: shipLocations.values()) {
			//get all the tiles each spot contains. See if the overlap.
			SimpleVector[] shipTiles = getShipsTiles(sp);
			for(SimpleVector i: testSpotTiles) {
				for(SimpleVector j: shipTiles) {
					if(i.x == j.x && i.y == j.y) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private SimpleVector[] getShipsTiles(ShipPos pos) {
		SimpleVector start = pos.pos1;
		SimpleVector end = pos.pos2;
		SimpleVector[] toReturn;
		if(start.x == end.x) {
			toReturn = new SimpleVector[Math.max(start.y, end.y) - Math.min(start.y, end.y) + 1];
			for(int i = 0; i < toReturn.length; i++) {
				toReturn[i] = new SimpleVector(start.x, Math.min(start.y, end.y)+i);
			}
		} else {
			toReturn = new SimpleVector[Math.max(start.x, end.x) - Math.min(start.x, end.x) + 1];
			for(int i = 0; i < toReturn.length; i++) {
				toReturn[i] = new SimpleVector(Math.min(start.x, end.x)+i, start.y);
			}
		}
		return toReturn;
	}


	public boolean isOver() {
		boolean flag = true;
		for(Ship s: shipLocations.keySet()) {
			for(SimpleVector v: getShipsTiles(shipLocations.get(s))) {
				if(missileTiles[v.x][v.y] == MissileTile.EMPTY) {
					flag = false;
					break;
				}
			}
			if(!flag) {
				break;
			}
		}
		return flag;
	}

	public ShootResult shoot(int x, int y) {
		Ship hitShip = null;
		if(missileTiles[x][y] != MissileTile.EMPTY) {
			return null;
		}
		for(Ship s: shipLocations.keySet()) {
			for(SimpleVector v: getShipsTiles(shipLocations.get(s))) {
				if(v.x == x && v.y == y) {
					missileTiles[v.x][v.y] = MissileTile.HIT;
					hitShip = s;
					break;
				}
			}
		}

		boolean dead = true;

		if(missileTiles[x][y] == MissileTile.EMPTY) {
			missileTiles[x][y] = MissileTile.MISS;
		} else if(hitShip != null) {
			for(SimpleVector v: getShipsTiles(shipLocations.get(hitShip))) {
				if(missileTiles[v.x][v.y] != MissileTile.HIT) {
					dead = false;
					break;
				}
			}
		} else {
			dead = false;
		}
		shootCount++;
		return new ShootResult(missileTiles[x][y], new SimpleVector(x, y), dead?hitShip:null);
	}

	public class ShootResult {
		private MissileTile tile;
		private Ship ship;
		private SimpleVector coords;
		public ShootResult(MissileTile tile, SimpleVector coords, Ship ship) {
			this.tile = tile;
			this.coords = coords;
			this.ship = ship;
		}

		public ShootResult(MissileTile tile, SimpleVector coords) {
			this(tile, coords, null);
		}

		public MissileTile getTile() {
			return tile;
		}

		public SimpleVector getCoords() { return coords; }

		public Ship getShip() {
			return ship;
		}

		public String toString() {
			if(ship == null) {
				return tile.name();
			} else {
				return tile.name() + " and killed " + ship.name();
			}
		}
	}

	public enum MissileTile {
		EMPTY, MISS, HIT
	}

	public enum Ship {
		AIRCRAFT_CARRIER(5), BATTLESHIP(4), SUBMARINE(3), DESTROYER(3), PATROL_BOAT(2);

		public int size;
		Ship(int size) {
			this.size = size;
		}
	}

	public class ShipPos {
		private SimpleVector pos1, pos2;
		public ShipPos(int x1, int y1, int x2, int y2) {
			pos1 = new SimpleVector(x1, y1);
			pos2 = new SimpleVector(x2, y2);
		}

		public SimpleVector getPos1() { return pos1; }
		public SimpleVector getPos2() { return pos2; }

		public String toString() {
			return String.format("%s | %s", pos1, pos2);
		}
	}

	public class SimpleVector {
		private int x, y;
		public SimpleVector(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		public String toString() {
			return String.format("%d,%d", x, y);
		}
	}
}
