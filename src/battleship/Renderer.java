package battleship;

import processing.core.PApplet;
import processing.core.PConstants;

/**
 * Created by Nathaniel on 12/14/2014
 * for battleship
 */
public class Renderer extends PApplet {

	private final int xSize = 660;
	private final int ySize = 660;
	private GameMain game;

	public Renderer() {
		runSketch();
	}

	public void setGame(GameMain gm) {
		game = gm;
	}

	public void setup() {
		this.size(xSize+1, ySize+1);
		this.background(0);
	}

	public void draw() {
		this.background(0);
		renderGuide(this);
		renderShips(this);
		for(int i = 0; i < 10; i++) {
			for(int j = 0; j < 10; j++) {
				renderTile(this, i, j);
			}
		}
	}

	private void renderGuide(PApplet p) {
		if(game == null) {
			return;
		}
		String horizontal = "ABCDEFGHIJKLMN";
		String vertical = "0123456789";
		int tileSizeX = xSize/11;
		int tileSizeY = ySize/11;

		p.stroke(0, 150, 0);
		int temp;
		for(int i = 0; i < 12; i++) {
			temp = tileSizeX*i;
			p.line(temp, 0, temp, ySize);
			temp = tileSizeY*i;
			p.line(0, temp, xSize, temp);
		}

		p.textSize(48);
		p.textAlign(PConstants.CENTER, PConstants.CENTER);
		p.fill(0, 255, 0);
		for(int i = 0; i < 10; i++) {
			p.text(horizontal.charAt(i), tileSizeX + (tileSizeX * i) + (tileSizeX / 2), (tileSizeY / 2) - (tileSizeY / 7));
			p.text(vertical.charAt(i), tileSizeX / 2, tileSizeY + (tileSizeY * i) + (tileSizeY / 2) - (tileSizeY / 7));
		}

		p.textSize(12);
		p.text(game.getShootCount(), tileSizeX/2, tileSizeY/2-(tileSizeY/8));
	}

	private void renderTile(PApplet p, int x, int y) {
		if(game == null) {
			return;
		}
		int tileSizeX = xSize/11;
		int tileSizeY = ySize/11;
		p.stroke(0, 255, 0);
		if(!game.getMissileTiles()[x][y].equals(GameMain.MissileTile.EMPTY)) {
			for(int i = 0; i < 3; i++) {
				p.line(
						tileSizeX + (tileSizeX * x),
						tileSizeY + (tileSizeY * y) + ((tileSizeY/3) * i),
						tileSizeX + (tileSizeX * x) + ((tileSizeX/3) * i),
						tileSizeY + (tileSizeY * y)
				);
			}
			for(int i = 0; i < 3; i++) {
				p.line(
						tileSizeX*2 + (tileSizeX * x),
						tileSizeY + (tileSizeY * y) + ((tileSizeY/3) * i),
						tileSizeX + (tileSizeX * x) + ((tileSizeX/3) * i),
						tileSizeY*2 + (tileSizeY * y)
				);
			}
			//p.ellipse(tileSizeX + (tileSizeX * x) + (tileSizeX / 2), tileSizeY + (tileSizeY * y) + (tileSizeY / 2), 20, 20);
		}
	}

	private void renderShips(PApplet p) {
		if(game == null) {
			return;
		}
		p.fill(0);
		p.stroke(0, 255, 0);
		int tileSizeX = xSize/11;
		int tileSizeY = ySize/11;
		GameMain.ShipPos shipPos;
		for(GameMain.Ship ship: game.getShipLocations().keySet()) {
			shipPos = game.getShipLocations().get(ship);
			int xPos = (Math.min(shipPos.getPos1().getX(), shipPos.getPos2().getX())*tileSizeX+tileSizeX) + ((Math.max(shipPos.getPos1().getX(), shipPos.getPos2().getX())*tileSizeX - Math.min(shipPos.getPos1().getX(), shipPos.getPos2().getX())*tileSizeX)/2) + tileSizeX/2;
			int yPos = (Math.min(shipPos.getPos1().getY(), shipPos.getPos2().getY())*tileSizeY+tileSizeY) + ((Math.max(shipPos.getPos1().getY(), shipPos.getPos2().getY())*tileSizeY - Math.min(shipPos.getPos1().getY(), shipPos.getPos2().getY())*tileSizeY)/2) + tileSizeY/2;
			int xSize = Math.abs(shipPos.getPos1().getX() - shipPos.getPos2().getX())*tileSizeY+30;
			int ySize = Math.abs(shipPos.getPos1().getY() - shipPos.getPos2().getY())*tileSizeX+30;
			p.ellipse(xPos, yPos, xSize, ySize);
		}
	}
}
