package ai;

/**
 * Created by Nathaniel on 12/12/2014
 * for Battleship
 */
public class Shot {
	private int x;
	private int y;
	public Shot(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public String toString() {
		return String.format("%d,%d", x, y);
	}
}