package ai;

/**
 * Created by Nathaniel on 12/14/2014
 * for battleship
 */
public class SimpleVector {
	private int x, y;
	public SimpleVector(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public String toString() {
		return String.format("%d,%d", x, y);
	}
}
