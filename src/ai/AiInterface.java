package ai;

/**
 * Created by nathaniel on 12/12/14
 */
public interface AiInterface {
    public Shot getShot();
    public void handleResult(ShotResult result);
}
