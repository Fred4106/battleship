package ai;

/**
 * Created by Nathaniel on 12/12/2014
 * for Battleship
 */
public class ShotResult {

	private Shot shot;
	private ShipType ship;
	private boolean hit;

	public ShotResult(Shot shot, boolean hit, ShipType ship) {
		this.shot = shot;
		this.hit = hit;
		this.ship = ship;
	}

	public Shot getShot() {
		return shot;
	}

	public boolean getHit() {
		return hit;
	}

	public ShipType getShip() {
		return ship;
	}

	public String toString() {
		if(ship != null) {
			return String.format("Shot %s %s %s", shot, (hit)?"hit":"miss", ship);
		} else {
			return String.format("Shot %s %s", shot, (hit)?"hit":"miss");
		}
	}
}
