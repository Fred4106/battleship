package ai;

/**
 * Created by Nathaniel on 12/12/2014
 * for Battleship
 */
public enum TileType {
	EMPTY, MISS, HIT, SUNK, SUNK_AIRCRAFT_CARRIER, SUNK_BATTLESHIP, SUNK_SUBMARINE, SUNK_DESTROYER, SUNK_PATROL_BOAT
}