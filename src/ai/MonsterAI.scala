package ai

import java.util.Random

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Map

/**
 * Created by Nathaniel on 12/14/2014
 * for battleship
 */
class MonsterAI extends AiInterface {
	var tilesArray: Array[Array[TileType]] = Array.fill[TileType](10, 10) {TileType.EMPTY}
	val random: Random = new Random()

	var shipPosBuffer: ArrayBuffer[Map[ShipType, ShipPos]] = findAllPos()

	override def getShot: Shot = {
		val buf = getShots
		buf(random.nextInt(buf.size))
	}

	override def handleResult(result: ShotResult): Unit = {
		if(result.getShip == null) {
			tilesArray(result.getShot.getX)(result.getShot.getY) = if(result.getHit) {TileType.HIT} else {TileType.MISS}
		} else {
			tilesArray(result.getShot.getX)(result.getShot.getY) = result.getShip match {
				case ShipType.AIRCRAFT_CARRIER => TileType.SUNK_AIRCRAFT_CARRIER
				case ShipType.BATTLESHIP => TileType.SUNK_BATTLESHIP
				case ShipType.SUBMARINE => TileType.SUNK_SUBMARINE
				case ShipType.DESTROYER => TileType.SUNK_DESTROYER
				case ShipType.PATROL_BOAT => TileType.SUNK_PATROL_BOAT
			}
		}
	}

	private def findAllPos(): ArrayBuffer[Map[ShipType, ShipPos]] = {
		var buf = stripDuplicates(findAllPosForShip(ShipType.AIRCRAFT_CARRIER))
		//buf = mergePosForShip(buf, findAllPosForShip(ShipType.BATTLESHIP))
		//buf = mergePosForShip(buf, findAllPosForShip(ShipType.SUBMARINE))
		//buf = mergePosForShip(buf, findAllPosForShip(ShipType.DESTROYER))
		//buf = mergePosForShip(buf, findAllPosForShip(ShipType.PATROL_BOAT))
		buf
	}

	private def stripDuplicates(buf: ArrayBuffer[Map[ShipType, ShipPos]]): ArrayBuffer[Map[ShipType, ShipPos]] = {
		class BetterShipPos(val s1: ShipPos) {
			def ==(s2: ShipPos): Boolean = {
				if(s1.getPos1.getX == s2.getPos1.getX) {
					if(s1.getPos1.getY == s2.getPos1.getY) {
						if(s1.getPos2.getX == s2.getPos2.getX) {
							if(s1.getPos2.getY == s2.getPos2.getY) {
								true
							}
						}
					}
				}
				if(s1.getPos1.getX == s2.getPos2.getX) {
					if(s1.getPos1.getY == s2.getPos2.getY) {
						if(s1.getPos2.getX == s2.getPos1.getX) {
							if(s1.getPos2.getY == s2.getPos1.getY) {
								true
							}
						}
					}
				}
				false
			}
		}
		implicit def spToSp(s: ShipPos): BetterShipPos = new BetterShipPos(s)
		val tempBuf: ArrayBuffer[Map[ShipType, ShipPos]] = new ArrayBuffer[mutable.Map[ShipType, ShipPos]]()
		buf.copyToBuffer(tempBuf)
		for(i <- 0 until tempBuf.size) {
			for(j <- i+1 until tempBuf.size) {
				for(k <- buf(i).keys; l <- buf(j).keys) {
					if(tempBuf(i)(k) == tempBuf(j)(l)) {
						tempBuf.remove(j)
					}
				}
			}
		}
		tempBuf
	}

	private def mergePosForShip(m1: ArrayBuffer[Map[ShipType, ShipPos]], m2: ArrayBuffer[Map[ShipType, ShipPos]]): ArrayBuffer[Map[ShipType, ShipPos]] = {
		val toReturn: ArrayBuffer[Map[ShipType, ShipPos]] = new ArrayBuffer[mutable.Map[ShipType, ShipPos]]()
		var tempMap: Map[ShipType, ShipPos] = null
		for(i <- m1; j <- m2) {
			for(k <- i.keys; l <- j.keys) {
				tempMap = Map[ShipType, ShipPos]()
				if(!doCollide(i(k), j(l))) {
					tempMap.put(k, i(k))
					tempMap.put(l, j(l))
					toReturn += tempMap
				}
			}
		}
		toReturn
	}

	private def findAllPosForShip(ship: ShipType): ArrayBuffer[Map[ShipType, ShipPos]] = {
		var mainSpot: SimpleVector = null
		val shipPos: ArrayBuffer[Map[ShipType, ShipPos]] = new ArrayBuffer[Map[ShipType, ShipPos]]()
		var ts1: SimpleVector = null
		var ts2: SimpleVector = null
		var ts3: SimpleVector = null
		var ts4: SimpleVector = null
		var sp1: ShipPos = null
		var sp2: ShipPos = null
		var sp3: ShipPos = null
		var sp4: ShipPos = null
		var tempMap: Map[ShipType, ShipPos] = null
		for(i <- 0 to 9; j <- 0 to 9) {
			mainSpot = new SimpleVector(i, j)
			ts1 = new SimpleVector(mainSpot.getX-ship.size, mainSpot.getY)
			ts2 = new SimpleVector(mainSpot.getX+ship.size, mainSpot.getY)
			ts3 = new SimpleVector(mainSpot.getX, mainSpot.getY-ship.size)
			ts4 = new SimpleVector(mainSpot.getX, mainSpot.getY+ship.size)
			sp1 = new ShipPos(mainSpot, ts1)
			sp2 = new ShipPos(mainSpot, ts2)
			sp3 = new ShipPos(mainSpot, ts3)
			sp4 = new ShipPos(mainSpot, ts4)
			if(isOnScreen(sp1)) {
				tempMap = Map[ShipType, ShipPos]()
				tempMap.put(ship, sp1)
				shipPos += tempMap
			}
			if(isOnScreen(sp2)) {
				tempMap = Map[ShipType, ShipPos]()
				tempMap.put(ship, sp2)
				shipPos += tempMap
			}
			if(isOnScreen(sp3)) {
				tempMap = Map[ShipType, ShipPos]()
				tempMap.put(ship, sp3)
				shipPos += tempMap
			}
			if(isOnScreen(sp4)) {
				tempMap = Map[ShipType, ShipPos]()
				tempMap.put(ship, sp4)
				shipPos += tempMap
			}
		}
		shipPos
	}

	private def doCollide(pos1: ShipPos, pos2: ShipPos): Boolean = {
		for(i <- getShipsTiles(pos1); j <- getShipsTiles(pos2)) {
			if(i.getX == j.getX && i.getY == j.getY) {
				true
			}
		}
		false
	}

	private def getShipsTiles(pos: ShipPos): Array[SimpleVector] = {
		val start = pos.getPos1
		val end = pos.getPos2
		var toReturn: Array[SimpleVector] = null
		if(start.getX == end.getX) {
			toReturn = new Array[SimpleVector](Math.max(start.getY, end.getY) - Math.min(start.getY, end.getY) + 1)
			for(i <- 0 until toReturn.length) {
				toReturn(i) = new SimpleVector(start.getX, Math.min(start.getY, end.getY) + 1)
			}
		} else {
			toReturn = new Array[SimpleVector](Math.max(start.getX, end.getX) - Math.min(start.getX, end.getX) + 1)
			for(i <- 0 until toReturn.length) {
				toReturn(i) = new SimpleVector(start.getX, Math.min(start.getX, end.getX) + 1)
			}
		}
		toReturn
	}

	private def isOnScreen(ship: ShipPos): Boolean = {
		if(ship.getPos1.getX < 0 || ship.getPos1.getX > 9) {
			false
		} else if(ship.getPos1.getY < 0 || ship.getPos1.getY > 9) {
			false
		} else if(ship.getPos2.getX < 0 || ship.getPos2.getX > 9) {
			false
		} else if(ship.getPos2.getY < 0 || ship.getPos2.getY > 9) {
			false
		} else {
			true
		}
	}

	private def getShots: ArrayBuffer[Shot] = {
		val buffer: ArrayBuffer[Shot] = new ArrayBuffer[Shot]()
		for(i <- 0 to 9; j <- 0 to 9) {
			buffer += new Shot(i, j)
		}
		buffer.filter(isEmpty)
	}

	private def isHit(shot: Shot): Boolean = tilesArray(shot.getX)(shot.getY).ordinal() >= TileType.HIT.ordinal()
	private def isEmpty(shot: Shot): Boolean = tilesArray(shot.getX)(shot.getY) == TileType.EMPTY
	private def isRealSpot(shot: Shot): Boolean = !(shot.getX < 0 || shot.getX > 9 || shot.getY < 0 || shot.getY > 9)
}