package ai;

/**
 * Created by Nathaniel on 12/12/2014
 * for Battleship
 */
public enum ShipType {
	AIRCRAFT_CARRIER(5), BATTLESHIP(4), SUBMARINE(3), DESTROYER(3), PATROL_BOAT(2);

	public int size;
	ShipType(int size) {
		this.size = size;
	}
	public String toString() {
		return this.name();
	}
}
