package ai;

/**
 * Created by Nathaniel on 12/14/2014
 * for battleship
 */
public class ShipPos {
	private SimpleVector pos1, pos2;
	public ShipPos(int x1, int y1, int x2, int y2) {
		pos1 = new SimpleVector(x1, y1);
		pos2 = new SimpleVector(x2, y2);
	}

	public ShipPos(SimpleVector v1, SimpleVector v2) {
		this(v1.getX(), v1.getY(), v2.getX(), v2.getY());
	}

	public SimpleVector getPos1() { return pos1; }
	public SimpleVector getPos2() { return pos2; }

	public String toString() {
		return String.format("%s | %s", pos1, pos2);
	}
}
