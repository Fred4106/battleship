package ai

import java.util.Random

import scala.collection.mutable.ArrayBuffer

/**
 * Created by Nathaniel on 12/12/2014
 * for Battleship
 */
class BattleAI extends AiInterface {

	var tilesArray: Array[Array[TileType]] = Array.fill[TileType](10, 10) {TileType.EMPTY}
	val random: Random = new Random()

	var logicBlocks: Array[Shot => Boolean] = Array(
		(x: Shot) => isLikelyTarget(x),
		(x: Shot) => xOrMore(x, 4, isHit),
		(x: Shot) => xOrMore(x, 3, isHit),
		(x: Shot) => xOrMore(x, 2, isHit),
		(x: Shot) => xOrMore(x, 1, isHit),
		(x: Shot) => xOrMore(x, 4, isEmpty),
		(x: Shot) => xOrMore(x, 3, isEmpty),
		(x: Shot) => xOrMore(x, 2, isEmpty),
		(x: Shot) => xOrMore(x, 1, isEmpty),
		Shot => true
	)

	override def getShot: Shot = {
		val buf = getShots
		var bufTemp: ArrayBuffer[Shot] = null
		var shot: Shot = null
		for(i <- 0 until logicBlocks.size; if shot == null) {
			bufTemp = buf.filter(logicBlocks(i).apply)
			if(bufTemp.size != 0) {
				shot = bufTemp(random.nextInt(bufTemp.size))
			}
		}
		shot
	}

	override def handleResult(result: ShotResult): Unit = {
		if(result.getShip == null) {
			tilesArray(result.getShot.getX)(result.getShot.getY) = if(result.getHit) {TileType.HIT} else {TileType.MISS}
		} else {
			tilesArray(result.getShot.getX)(result.getShot.getY) = result.getShip match {
				case ShipType.AIRCRAFT_CARRIER => TileType.SUNK_AIRCRAFT_CARRIER
				case ShipType.BATTLESHIP => TileType.SUNK_BATTLESHIP
				case ShipType.SUBMARINE => TileType.SUNK_SUBMARINE
				case ShipType.DESTROYER => TileType.SUNK_DESTROYER
				case ShipType.PATROL_BOAT => TileType.SUNK_PATROL_BOAT
			}
		}
	}

	def getShots: ArrayBuffer[Shot] = {
		val buffer: ArrayBuffer[Shot] = new ArrayBuffer[Shot]()
		for(i <- 0 to 9; j <- 0 to 9) {
			buffer += new Shot(i, j)
		}
		buffer.filter(isEmpty)
	}

	private def isLikelyTarget(shot: Shot): Boolean = {
		var s1 = new Shot(shot.getX-1, shot.getY)
		var s2 = new Shot(shot.getX+1, shot.getY)
		if(isRealSpot(s1) && isRealSpot(s2) && isHit(s1) && isHit(s2)) {
			true
		}
		s1 = new Shot(shot.getX, shot.getY-1)
		s2 = new Shot(shot.getX, shot.getY+1)
		if(isRealSpot(s1) && isRealSpot(s2) && isHit(s1) && isHit(s2)) {
			true
		}
		false
	}

	private def xOrMore(shot: Shot, num: Int, func: (Shot => Boolean)): Boolean = {
		var count = 0
		val s1 = new Shot(shot.getX-1, shot.getY)
		val s2 = new Shot(shot.getX+1, shot.getY)
		val s3 = new Shot(shot.getX, shot.getY-1)
		val s4 = new Shot(shot.getX, shot.getY+1)

		if(isRealSpot(s1) && func.apply(s1)) count += 1
		if(isRealSpot(s2) && func.apply(s2)) count += 1
		if(isRealSpot(s3) && func.apply(s3)) count += 1
		if(isRealSpot(s4) && func.apply(s4)) count += 1

		count >= num
	}

	private def isHit(shot: Shot): Boolean = tilesArray(shot.getX)(shot.getY).ordinal() >= TileType.HIT.ordinal()

	private def isEmpty(shot: Shot): Boolean = tilesArray(shot.getX)(shot.getY) == TileType.EMPTY

	private def isRealSpot(shot: Shot): Boolean = !(shot.getX < 0 || shot.getX > 9 || shot.getY < 0 || shot.getY > 9)

}